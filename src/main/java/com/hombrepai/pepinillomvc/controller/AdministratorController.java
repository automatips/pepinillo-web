package com.hombrepai.pepinillomvc.controller;

import com.hombrepai.pepinillomvc.model.Player;
import com.hombrepai.pepinillomvc.model.Administrator;
import com.hombrepai.pepinillomvc.repository.AdministratorRepository;
import com.hombrepai.pepinillomvc.repository.PlayerRepository;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdministratorController {
    
    @Autowired
    private PlayerRepository playerRepository;
    
    @Autowired
    private AdministratorRepository administratorRepository;

    @Autowired
    public void setAdministratorRepository(AdministratorRepository administratorRepository) {
        this.administratorRepository = administratorRepository;
    }

    @RequestMapping("/login")
    public String login(
        @RequestParam(value = "username", required = false) String username,
        @RequestParam(value = "password", required = false) String password,
        Model model,
        HttpSession session
    ) {
        Administrator resultado = administratorRepository.findByUser(username);
        if (
            resultado != null
            ){
            if (resultado.getPassword().equals(password)){                
                List<Player> players = playerRepository.findByAdministrator(resultado.getUser());
                
                session.setAttribute("players", players);
                session.setAttribute("administrator", resultado);
                session.setAttribute("inSession", true);
                
                model.addAttribute("players", players);                    
                model.addAttribute("administrator", resultado);
                model.addAttribute("inSession", true);
                return "players";
            }
            else{
                session.setAttribute("players", null);
                session.setAttribute("administrator", null);
                session.setAttribute("inSession", null);
                
                model.addAttribute("players",null);
                model.addAttribute("administrator", null);
                model.addAttribute("inSession", false);
                return "redirect:/templates/error/access-denied";
            }
        }
        else{
            session.setAttribute("players", null);
            session.setAttribute("administrator", null);
            session.setAttribute("inSession", null);

            model.addAttribute("players",null);
            model.addAttribute("administrator", null);
            model.addAttribute("inSession", false);
            return "redirect:/templates/error/access-denied";
        }
    }
    
    @RequestMapping("/logout")
    public String logout(
        Model model, HttpSession session
    ) {        
        session.setAttribute("players", null);
        session.setAttribute("administrator", null);
        session.setAttribute("inSession", null);

        model.addAttribute("players",null);
        model.addAttribute("administrator", null);
        model.addAttribute("inSession", false);
        return "index"; 
    }
}
