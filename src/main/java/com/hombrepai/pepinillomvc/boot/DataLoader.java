package com.hombrepai.pepinillomvc.boot;

import com.hombrepai.pepinillomvc.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {

    private PlayerRepository playerRepository;
    @Autowired
    public void setPlayerRepository(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void run(String... strings) throws Exception {

    }
}
